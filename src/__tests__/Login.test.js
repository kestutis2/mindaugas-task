import React from 'react';
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import { shallow, mount } from 'enzyme';
import "../setupTests";

import Login from '../components/Auth/Login';

const mockStore = configureMockStore();
const store = mockStore({});

const wrapper = mount(
			<Provider store={store}>
				<Login />
			</Provider>
		);

describe("Login Page Tests", () => {

	it("renders correctly", () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('should have a form', ()=> {	
		expect(wrapper.find('Form')).toHaveLength(1);
	});

	it('should have inputs for username & password', ()=> {
		expect(wrapper.find('FormControl')).toHaveLength(2);
	});

	it('should have a btn component', ()=> {
		expect(wrapper.find('Button')).toHaveLength(1);

		expect(wrapper.find('Button').text()).toEqual('Log In');
	});

	it('username check',() => {

		wrapper.find('FormControl[name="username"]').simulate('change', {target: {name: 'username', value: 'tesonet'}});
		expect(wrapper.find('FormControl[name="username"]').props().value).toBe('tesonet');
	});

	it('password check',() => {

		wrapper.find('FormControl[name="password"]').simulate('change', {target: {name: 'password', value: 'partyanimal'}});
		expect(wrapper.find('FormControl[name="password"]').props().value).toBe('partyanimal');
	});

	it('login check with wrong data',() => {
		const componentInstance = wrapper.find('Login').instance();

		wrapper.find('FormControl[name="username"]').simulate('change', {target: {name: 'username', value: 'tesonet'}});
		wrapper.find('FormControl[name="password"]').simulate('change', {target: {name: 'password', value: 'Partyanimal'}});
		wrapper.find('Button').simulate('click');
		wrapper.update();
		expect(componentInstance.props.loggedIn).toBe(undefined);
	});
  });