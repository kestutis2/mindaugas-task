import React from 'react';
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import { shallow, mount } from 'enzyme';
import "../setupTests";

import Servers from '../components/Servers/Servers';

const mockStore = configureMockStore();
const store = mockStore({});

const wrapper = mount(
			<Provider store={store}>
				<Servers />
			</Provider>
		);

describe("Servers Page Tests", () => {

	it("renders correctly", () => {
		expect(wrapper).toMatchSnapshot();
	});

    it('should have a table', ()=> {	
		expect(wrapper.find('table')).toHaveLength(1);
	});

    it('should have a logout button', ()=> {	
		expect(wrapper.find('Button')).toHaveLength(1);
	});
});