import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import axios from "axios";
import { connect } from "react-redux";
import { startLogin } from "../../store/actions";
import "../../scss/Login.scss";
import whiteLogo from '../../assets/logo-testio-white.svg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Spinner from 'react-bootstrap/Spinner';
import Alert from 'react-bootstrap/Alert';

class Login extends Component {
	constructor() {
		super();

		this.state = {
			username: "",
			password: "",
			errors: [],
			loading: false
		};
		this.handleChange = this.handleChange.bind(this);
		this.login = this.login.bind(this);
  	}

	hasError(key) {
		return this.state.errors.indexOf(key) !== -1;
	}

  	handleChange = e => this.setState({ [e.target.name]: e.target.value });

  	login = e => {
		e.preventDefault();

		let errors = [];

		if (this.state.username === "") {
		errors.push("username");
		}

		if (this.state.password === "") {
			errors.push("password");
		}

		this.setState({
			errors: errors
		});
	
		if (errors.length > 0) {
			return false;
		} else {
			this.setState({
				loading: true
			});
			const headers = {
				'Content-Type': 'application/json'
			}
			const data = this.state;
		
			axios.post(`https://playground.tesonet.lt/v1/tokens`, data , {
				headers: headers
			})  
			.then(res => {
				if (res.data.token) {
					this.props.login(data, res.data.token);
				}
			})
			.catch(error => {
				console.log(error);
				errors = [];
				errors.push("wrongUsernameOrPassword");
				this.setState({
					errors: errors
				});
				this.setState({
					loading: false
				});
			});
		}
  	};

  render() {
	  if (this.props.loggedIn) {
		return <Redirect to="/servers" />
	  }
		return (
		<div className="login-block">
			<Container className="h-100">
				<Row className="h-100">
					<Col className="text-center">
						<div className="form-block">
							<img className="app-logo" src={whiteLogo} width="245" alt="Logo" title="Logo" />

							<Form className="login-form mx-auto" onSubmit={this.login}>
								{this.hasError("wrongUsernameOrPassword") ? (
									<Alert variant="danger">
									Invalid username or password!
									</Alert>
								) : (
									''
								)}
								<Form.Row>
									<InputGroup 
										className={
											this.hasError("username")
												? "is-invalid"
												: "mb-3"
											}
											xs="12" xl={2}>
										<InputGroup.Prepend>
											<InputGroup.Text>
												<FontAwesomeIcon icon="user" />
											</InputGroup.Text>
										</InputGroup.Prepend>
										<FormControl
											type="text"
											name="username"
											className={
											this.hasError("username")
												? "is-invalid"
												: ""
											}
											placeholder="Username"
											value={this.state.username}
											onChange={this.handleChange} />
											{this.hasError("username") ? (
												<div
													className={
													this.hasError("username") ? "inline-errormsg" : "d-none"
													}
												>
													Please enter a Username
												</div>
										) : (
											''
										)}
									</InputGroup>

									<InputGroup
										className={
											this.hasError("password")
												? "is-invalid"
												: "mb-3"
											}
											xs="12" xl={2}>
										<InputGroup.Prepend>
											<InputGroup.Text>
												<FontAwesomeIcon icon="lock" />
											</InputGroup.Text>
										</InputGroup.Prepend>
										<FormControl
											type="password"
											name="password"
											className={
											this.hasError("password")
												? "is-invalid"
												: ""
											}
											placeholder="Password"
											value={this.state.password}
											onChange={this.handleChange} />
											{this.hasError("password") ? (
												<div
													className={
													this.hasError("password") ? "inline-errormsg" : "d-none"
													}
												>
													Please enter a Password
												</div>
										) : (
											''
										)}
									</InputGroup>

									<Button
										className="submit-btn w-100"
										variant="primary"
										type="submit"
										id="aaaa"
										xs="12" xl={2}>
										{this.state.loading ? (<Spinner
											className="mr-2"
											as="span"
											animation="border"
											size="sm"
											role="status"
											aria-hidden="true"
											/>) : ('')}
										Log In
									</Button>	
								</Form.Row>
							</Form>
						</div>
					</Col>
				</Row>
			</Container>
		</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		loggedIn: state.loggedIn,
		loginProcessing: state.loginProcessing
	};
};

const mapDispatchToProps = dispatch => {
	return {
		login: (data, token) => dispatch(startLogin(data, token))
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Login);
