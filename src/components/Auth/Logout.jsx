import React, {useEffect} from "react";
import {useDispatch} from "react-redux";

import { logout } from "../../store/actions";

export default () => {
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(logout());
	}, []);

	return (
		<div>Logging out...</div>
	)
};
