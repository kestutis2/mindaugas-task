import React from "react";
import { connect } from "react-redux";
import axios from 'axios';
import "../../scss/Servers.scss";
import darkLogo from '../../assets/logo-testio-dark.svg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';

class Servers extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			serversList: []
		};
	}

	componentDidMount() {
		axios.get('https://playground.tesonet.lt/v1/servers', {
		headers: {
			'Authorization': `${this.props.token}`
		}
		})
		.then((res) => {
			this.setState({serversList: res.data});
			this.setState({isLoading: false});
		})
		.catch((error) => {
		console.error(error);
		});
	}

	logout = () => {
		this.props.history.push("/logout");
	};
	render() {
		return (
			<React.Fragment>
				<Container className="header-wrapper" fluid>
					<div className="header-block clearfix">
						<img className="app-logo float-left" src={darkLogo} width="115" alt="Logo" title="Logo" />
						<Button
							className="logout-btn float-right rounded-0"
							variant="outline-primary"
							onClick={this.logout}>
								<FontAwesomeIcon icon="sign-out-alt" /> Logout
						</Button>
					</div>
				</Container>
				<div className="table-area">
					<Table className="tableFixHead mb-0" hover>
						<thead className="servers-table-head">
							<tr>
								<th>Server</th>
								<th className="text-right light-font">Distance</th>
							</tr>
						</thead>
						<tbody>
							{!this.state.isLoading ? (
								this.state.serversList.map((server, index) => {
								return (
									<tr key={index}>
									<td> {server.name}</td>
									<td className="text-right">{server.distance}</td>
									</tr>
								);
								})
							) : (
								<tr><td className="text-center">Loading...</td></tr>
							)}
						</tbody>
					</Table>
				</div>
			</React.Fragment>
		);
	}
}

function mapStateToProps(state) {
    return {
		token: state.token
    };
}

export default connect(mapStateToProps)(Servers);
