import {
	LOGIN,
	LOGINPROCESSING,
	LOGOUT
} from "./actionTypes.js";

export const startLogin = (content, token) => {
	return dispatch => {
		dispatch(loginProcessing());
		localStorage.setItem('authentication', token);
		dispatch(login(content, token));
	};
};

export const logout = () => dispatch => {
	localStorage.removeItem('authentication');
	return dispatch({
	  type: LOGOUT
	});
};

const loginProcessing = () => {
	return {
		type: LOGINPROCESSING,
		payload: ""
	};
};

const login = (content, token) => {
	return {
		type: LOGIN,
		payload: content,
		token: token
	};
};
