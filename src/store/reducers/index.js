import {
    LOGIN,
    LOGINPROCESSING,
	LOGOUT
  } from "../actions/actionTypes.js";
  
  export const reducer = (state = {}, action) => {
	const storageItem = localStorage.getItem('authentication');
	if (storageItem) {
		state = { ...state, loggedIn: true, token: storageItem };
	}

    switch (action.type) {
      	case LOGIN:
        	return { ...state, loggedIn: true, token: action.token };
        	break;
      	case LOGINPROCESSING:
        	return { ...state, loginProcessing: true };
        	break;
	  	case LOGOUT:
        	return { ...state, loggedIn: false, token: null };
        	break;
      	default:
        return state;
    }
  };
  