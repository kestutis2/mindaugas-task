import React from 'react';
import {BrowserRouter as Router, Route, Redirect} from "react-router-dom";
import { useSelector } from "react-redux";

import LoginPage from "./components/Auth/Login.jsx";
import LogoutPage from "./components/Auth/Logout.jsx";
import ServersPage from "./components/Servers/Servers.jsx";

const PrivateRoute = ({ component: Component, ...rest}) =>  {

	const loggedIn = useSelector(state => Boolean(state.loggedIn));

	return (
		<Route
		{...rest}
		render={props => (
			!loggedIn ? (
			<Redirect to={{ pathname: '/login', state: { from: props.location }
			}}/>
			) : (
			<Component {...props}/>
			)
		)}/>
	);
};

export default () => (
	<Router>
		<Route name="login" path="/login" component={LoginPage}/>
		<PrivateRoute name="logout" path="/logout" component={LogoutPage}/>
		<PrivateRoute name="servers" path="/servers" component={ServersPage}/>
		<Redirect from="/" to="servers"/>
	</Router>
);
